# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

“Address”/"Location" in Salesforce can also refer to the Address/Location standard object. When referencing the object in your Apex code, 
always use Schema.Address/Schema.Location instead of “Address”/"Location" to prevent confusion with the standard compound field. If referencing both 
the object and the standard field in the same snippet, you can differentiate between the two by using System.Address/System.Location  
for the field and Schema.Address/Schema.Location for the object.

