public with sharing class DemoController{

    public DemoController(){
        //To use Location and Address Salesforce objects in Apex class, you have to use Schema namespace with them otherwise it will 
        throw an error because it considers it as Location and Address Class instead of Salesforce Objects.
    
        Schema.Location location;
        Schema.Address address;
            
        location = [SELECT id FROM Location WHERE Preliminary_Application_Ref__c =: pApplication.id and LocationType = 'Mailing' limit 1];
                
        address = [SELECT id, Street, Street_2__c, City,State, LocationType, PostalCode FROM Address WHERE ParentId =: location.id 
                and LocationType = 'Mailing' limit 1];
        }
        
        // By default if we use Location and Address directly it will take them as Location and Address class, if both things are in 
        same class we can differentiate using Schema.Location for referencing Salesforce Object and System.Location for 
        referencing Location class. Example for Location and Address Class :
        
        Account[] records = [SELECT id, BillingAddress FROM Account LIMIT 10];
        
        for(Account acct : records) {
            Address addr = acct.BillingAddress;
            Double lat = addr.latitude;
            Double lon = addr.longitude;
            Location loc1 = Location.newInstance(30.1944,-97.6682);
            Double apexDist1 = addr.getDistance(loc1, 'mi');
            Double apexDist2 = loc1.getDistance(addr, 'mi');
        }
    }
}